import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import SignInEmailStartPage from "./pages/SignInEmailStartPage";
import SignInEmailFinishPage from "./pages/SignInEmailFinishPage";
import IntroductionPage from "./pages/IntroductionPage/IntroductionPage";
import routes from "./routes";
import { ThemeProvider } from "@material-ui/core";

import { theme } from "./lib/theme";
import { firebaseService } from "./lib/firebase/firebaseService";
import Authorize from "./components/Authorize";

function App() {
    return (
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <Switch>
                    <Route exact path={routes.signInEmailStart} children={() => <SignInEmailStartPage />} />
                    <Route exact path={routes.signInEmailFinish} children={() => <SignInEmailFinishPage />} />
                    <Route>
                        <Authorize>
                            <Switch>
                                <Route exact path={routes.home} children={() => <HomePage />} />
                                <Route exact path={routes.introduction} children={() => <IntroductionPage />} />
                            </Switch>
                        </Authorize>
                    </Route>
                </Switch>
            </BrowserRouter>
        </ThemeProvider>
    );
}

export default App;
