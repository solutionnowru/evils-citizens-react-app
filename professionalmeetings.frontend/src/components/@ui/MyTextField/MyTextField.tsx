import React, { forwardRef } from 'react';
import { withStyles, createStyles, Theme, TextFieldClassKey, TextField, TextFieldProps } from '@material-ui/core';
import clsx from 'clsx';

export type MyTextFieldClassKey = TextFieldClassKey;

export type MyTextFieldProps = TextFieldProps & {

}

const styles = (theme: Theme) => createStyles<MyTextFieldClassKey, MyTextFieldProps>({
  root: {
    backgroundColor: '#F6F6F8',
    width: '100%',
    '& .MuiOutlinedInput-input': {
      padding: '12.5px 14px',
    },
    '& .MuiInputLabel-outlined': {
      transform: 'translate(14px, 15px) scale(1)',
    },
    '& .MuiInputLabel-outlined.MuiInputLabel-shrink': {
      transform: ' translate(14px, -6px) scale(0.75)',
    }
  }
});

const MyTextField = forwardRef<HTMLDivElement, MyTextFieldProps>((props, ref): JSX.Element => {
  const { classes, className, ...other } = props;

  return (
    <>
      <TextField className={clsx(className, classes?.root)} {...other} ref={ref}
        variant="outlined"
      />
    </>
  );
});

export default withStyles(styles, { name: 'MyTextField' })(MyTextField);
