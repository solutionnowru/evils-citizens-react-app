import React, { ReactElement, useState, useEffect } from "react";
import { firebaseService } from "../lib/firebase/firebaseService";
import { Redirect } from "react-router-dom";
import routes from "../routes";
import { UserData } from "../lib/UserData";

const Authorize: React.FC<{ children: ReactElement }> = (props) => {
    const [user, setUser] = useState<UserData | null | undefined>();

    useEffect(() => {
        firebaseService.onAuthStateChanged.subscribe((user) => {
            setUser(user);
        });
    }, []);

    if (user === null) return <Redirect to={routes.signInEmailStart} />;
    else return props.children;
};

export default Authorize;
