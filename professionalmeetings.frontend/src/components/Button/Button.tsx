import React, { forwardRef, RefAttributes } from 'react';
import { withStyles, StandardProps, createStyles, Theme } from '@material-ui/core';
import { Button as MuiButton, ButtonProps as MuiButtonProps, ButtonClassKey as MuiButtonClassKey } from '@material-ui/core';
import clsx from 'clsx';

export type ButtonClassKey = MuiButtonClassKey | 'root';

export interface ButtonProps extends
  StandardProps<MuiButtonProps & RefAttributes<HTMLButtonElement>, ButtonClassKey> {
}

const styles = (theme: Theme) => createStyles<ButtonClassKey, ButtonProps>({
  root: {
  },
  colorInherit: {},
  contained: {},
  containedPrimary: {},
  containedSecondary: {},
  containedSizeLarge: {},
  containedSizeSmall: {},
  disableElevation: {},
  disabled: {},
  endIcon: {},
  focusVisible: {},
  fullWidth: {},
  iconSizeLarge: {},
  iconSizeMedium: {},
  iconSizeSmall: {},
  label: {},
  outlined: {},
  outlinedPrimary: {},
  outlinedSecondary: {},
  outlinedSizeLarge: {},
  outlinedSizeSmall: {},
  sizeLarge: {},
  sizeSmall: {},
  startIcon: {},
  text: {},
  textPrimary: {},
  textSecondary: {},
  textSizeLarge: {},
  textSizeSmall: {}
});

const Button = forwardRef<HTMLButtonElement, ButtonProps>((props, ref): JSX.Element => {
  const { classes, className, children, ...other } = props;

  return (
    <MuiButton className={clsx(className)} classes={classes} {...other} ref={ref}>
      {children}
    </MuiButton>
  );
});

export default withStyles(styles, { name: 'Button' })(Button);
