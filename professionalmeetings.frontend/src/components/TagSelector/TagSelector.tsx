import React, { useEffect, useState, useRef } from "react";
import { Container, Chip, makeStyles, TextField, Input } from "@material-ui/core";
import _ from "lodash";
import clsx from "clsx";
import ClearIcon from "@material-ui/icons/Clear";
import AddIcon from "@material-ui/icons/Add";
import { object, string } from "yup";
import { MyTextField } from "../@ui/MyTextField";

export interface Tag {
    tag: string;
    group?: string;
}

interface TagSelectorProps {
    tags: Tag[];
    isTagSelected?: (tag: Tag) => void;
    onTagsSelected: (tags: Tag[]) => void;
    onTagAdded: (tag: string) => void;
}

const useStyles = makeStyles((theme) => ({
    tagGroupContainer: {
        textAlign: "left",
        marginTop: "20px",
        padding: "0",
    },
    tagGroup: {
        display: "flex",
        flexWrap: "wrap",
        padding: "0",
    },
    tag: {
        margin: "4px 4px 0 0",
        borderRadius: "4px",
        // color: theme.palette.grey[500],
        fontWeight: "bolder",
        cursor: "pointer",
        backgroundColor: '#ECF6F6',
        color: '#85C5C4',
    },
    selectedTag: {
        backgroundColor: '#85C5C4',
        color: theme.palette.primary.contrastText,
        "&:focus": {
            backgroundColor: '#85C5C4',
        },
    },
    addNewTagIcon: {
        cursor: "pointer",
    },
}));

const TagSelector: React.FC<TagSelectorProps> = (props) => {
    const [selectedTags, setSelectedTags] = useState<Tag[]>();
    const [newTag, setNewTag] = useState<string>("");
    const [newTagValidationMessage, setNewTagValidationMessage] = useState<string>();
    const classes = useStyles();
    const newTagValidationScheme = object<{ tag: string }>({
        tag: string()
            .required("Поле ввода пусто:(")
            .test("is-duplicate", "Такой тэг уже есть", (value) => props.tags.find((t) => t.tag === value) === undefined),
    });

    useEffect(() => {
        if (selectedTags) {
            props.onTagsSelected(selectedTags);
        }
    }, [selectedTags]);

    useEffect(() => {
        if (props.isTagSelected) {
            setSelectedTags(props.tags.filter((t) => props.isTagSelected!(t)));
        }
    }, [props.tags]);

    const userTagKey = "Пользовательский тег";
    const groups = _(props.tags)
        .groupBy((t) => t.group || userTagKey)
        .value();

    const addTag = () => {
        newTagValidationScheme
            .validate({ tag: newTag })
            .then((value) => {
                props.onTagAdded(value.tag);
                setNewTag("");
            })
            .catch((reason) => {
                if (reason.message) setNewTagValidationMessage(reason.message);
                else setNewTagValidationMessage("Тэг не прошел валидацию");
            });
    };

    const renderTags = (tags: Tag[]) => (
        <Container className={classes.tagGroup}>
            {_(tags)
                .orderBy((t) => t.tag)
                .map((t, i) => {
                    const isSelected = selectedTags !== undefined && selectedTags.indexOf(t) >= 0;
                    return (
                        <Chip
                            onClick={() => {
                                if (selectedTags === undefined) {
                                    setSelectedTags([t]);
                                } else {
                                    if (isSelected) setSelectedTags(selectedTags.filter((st) => st !== t));
                                    else setSelectedTags([...selectedTags, t]);
                                }
                            }}
                            className={clsx(isSelected ? [classes.tag, classes.selectedTag] : [classes.tag])}
                            key={i}
                            label={t.tag}
                            style={{}}
                        />
                    );
                })
                .value()}
        </Container>
    );

    return (
        <Container>
            {_(groups)
                .keys()
                .filter((key) => key !== userTagKey)
                .map((key) => {
                    return (
                        <Container key={key} className={classes.tagGroupContainer}>
                            {key}
                            {renderTags(groups[key])}
                        </Container>
                    );
                })
                .value()}
            <Container className={classes.tagGroupContainer}>
                <MyTextField
                    error={newTagValidationMessage !== undefined}
                    helperText={newTagValidationMessage}
                    value={newTag}
                    onKeyDown={(e) => {
                        // enter
                        if (e.keyCode === 13) {
                            addTag();
                        }
                    }}
                    onChange={(e) => {
                        setNewTag(e.target.value);
                        setNewTagValidationMessage(undefined);
                    }}
                    label="Свой тег"
                    InputProps={{
                        endAdornment: <AddIcon className={classes.addNewTagIcon} onClick={addTag} />,
                    }}
                />
                {renderTags(groups[userTagKey])}
            </Container>
        </Container>
    );
};

export default TagSelector;
