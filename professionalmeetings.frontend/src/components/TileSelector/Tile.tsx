import React from "react";
import { Paper, Typography } from "@material-ui/core";

export interface TileData {
  label: string;
  iconUrl: string;
}

interface TileProps {
  className: string;
  tileData: TileData;
  onClick: () => void;
}

const Tile: React.FC<TileProps> = (props) => {
  return (
    <Paper className={props.className} onClick={props.onClick}>
      <img src={props.tileData.iconUrl} alt={props.tileData.label} />
      <Typography variant="caption">{props.tileData.label}</Typography>
    </Paper>
  );
};

export default Tile;
