import React, { useState, useEffect } from "react";
import { TileData } from "./Tile";
import Tile from "./Tile";
import { Container, makeStyles, Theme, createStyles } from "@material-ui/core";
import { CSSProperties } from "@material-ui/core/styles/withStyles";

interface TileSelectorProps {
    tileStyles: CSSProperties;
    selectedTileStyles?: CSSProperties;
    styles: CSSProperties;
    columnCount: number;
    tiles: TileData[];
    isTileSelected?: (tile: TileData) => boolean;
    maximumSelectedTileCount?: number;
    onTileSelected: (tiles: TileData[]) => void;
}

const useStyles = makeStyles<Theme, TileSelectorProps, "tile" | "selectedTile" | "tileSelector">((theme: Theme) =>
    createStyles({
        tile: (props) => ({
            display: "grid",
            gridTemplateRows: "4fr 1fr",
            justifyItems: "center",
            cursor: "pointer",
            "& > img": {
                height: "100%",
            },
            ...props.tileStyles,
        }),
        selectedTile: (props) => ({
            ...props.selectedTileStyles,
            backgroundColor: "lightgray",
        }),
        tileSelector: (props) => ({
            ...props.styles,
            display: "grid",
            gridTemplateColumns: "1fr ".repeat(props.columnCount),
            gridAutoRows: "1fr",
        }),
    })
);

const TileSelector: React.FC<TileSelectorProps> = (props) => {
    const classes = useStyles(props);
    const [selectedTiles, setSelectedTiles] = useState<TileData[]>();

    useEffect(() => {
        if (selectedTiles) {
            props.onTileSelected(selectedTiles);
        }
    }, [selectedTiles]);

    useEffect(() => {
        if (props.isTileSelected) {
            setSelectedTiles(props.tiles.filter((t) => props.isTileSelected!(t)));
        }
    }, [props.tiles]);

    return (
        <Container className={classes.tileSelector}>
            {props.tiles.map((t, i) => (
                <Tile
                    className={`${classes.tile} ${selectedTiles && selectedTiles.indexOf(t) >= 0 ? classes.selectedTile : null}`}
                    key={i}
                    tileData={t}
                    onClick={() => {
                        if (selectedTiles === undefined) {
                            setSelectedTiles([t]);
                        } else {
                            if (selectedTiles.indexOf(t) >= 0) setSelectedTiles(selectedTiles.filter((st) => st !== t));
                            else if (selectedTiles.length < (props.maximumSelectedTileCount || 1)) setSelectedTiles([...selectedTiles, t]);
                        }
                    }}
                />
            ))}
        </Container>
    );
};

export default TileSelector;
