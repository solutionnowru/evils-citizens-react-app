export interface ConversationSubject {
    title: string;
    subject?: string;
}

export interface PersonData {
    firstName?: string;
    lastName?: string;
    invitationCode?: string;
    conversationSubjects?: ConversationSubject[];
    interests?: string[];
    profileImageUrl?: string;
    instagram?: string;
    twitter?: string;
    facebook?: string;
    status?: string;
}

export interface UserData {
    id: string;
    email: string;
    cityName?: string;
    objectives?: string[];
    personData: PersonData;
}

export function userHasAllRequiredFields(data: UserData) {
    return data.personData && data.personData.firstName && data.personData.lastName && data.cityName && data.objectives && data.objectives.length > 0;
}

export function resolveValidationMessage(data: UserData) {
    const fields = [];
    if (!data.personData.firstName) fields.push("Имя");
    if (!data.personData.lastName) fields.push("Фамилия");
    if (!data.cityName) fields.push("Город");
    if (!data.objectives || !data.objectives.length) fields.push("Цели");

    return "Поля, которые необходимо заполнить: " + fields.join(", ");
}
