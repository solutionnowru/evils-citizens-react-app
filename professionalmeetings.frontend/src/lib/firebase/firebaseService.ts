import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/storage";

import { firebaseConfig } from "./firebaseConfig";
import { Subject, from } from "rxjs";
import { tap } from "rxjs/operators";
import { UserData } from "../UserData";

class FirebaseService {
  public RecaptchaVerifier = firebase.auth.RecaptchaVerifier;
  public onAuthStateChanged = new Subject<UserData | undefined>();
  public fs: firebase.firestore.Firestore | null = null;
  public auth: firebase.auth.Auth | null = null;
  public func: firebase.functions.Functions | null = null;
  public storage: firebase.storage.Storage | null = null;

  public init(): void {
    if (firebase.apps.length) {
      return;
    }
    firebase.initializeApp(firebaseConfig);
    this.storage = firebase.storage();
    this.fs = firebase.firestore();
    this.func = firebase.functions();
    this.auth = firebase.auth();
    this.auth.useDeviceLanguage();
    this.auth.onAuthStateChanged((user: firebase.User | null): void => {
      if (!user) {
        this.onAuthStateChanged.next(undefined);
        return;
      }

      from(this.getUserData()).subscribe((userData: UserData | undefined): void => this.onAuthStateChanged.next(userData));
    });

    this.onAuthStateChanged.subscribe(console.log);
  }

  public chekIsAuthenticated(): boolean {
    return !!this.auth?.currentUser;
  }

  public logOut(): void {
    this.auth?.signOut();
    this.onAuthStateChanged.next(undefined);
  }

  public async sendSignInLinkToEmail(email: string, invite?: string, readypay?: string) {
    await this.auth?.sendSignInLinkToEmail(email, { url: `https://evils-citizens.web.app/sign-in-email-link-finish?email=${email}${invite ? `&invite=${invite}` : ''}${readypay ? `&readypay=${readypay}` : ''}`, handleCodeInApp: true });

    window.localStorage.setItem("emailForSignIn", email);
  }

  public finishSugnInEmalLink() {
    let email = window.localStorage.getItem("emailForSignIn");
    if (!email) {
      email = new URL(window.location.href).searchParams.get("email");
    }
    // Confirm the link is a sign-in with email link.
    if (firebase.auth().isSignInWithEmailLink(window.location.href)) {
      // Additional state parameters can also be passed via URL.
      // This can be used to continue the user's intended action before triggering
      // the sign-in operation.
      // Get the email if available. This should be available if the user completes
      // the flow on the same device where they started it.

      if (!email) {
        // User opened the link on a different device. To prevent session fixation
        // attacks, ask the user to provide the associated email again. For example:
        email = window.prompt("Please provide your email for confirmation");
      }

      if (!email) {
        return;
      }
      // The client SDK will parse the code from the link for you.
      return this.auth
        ?.signInWithEmailLink(email, window.location.href)
        .then(function (result) {
          // Clear email from storage.
          window.localStorage.removeItem("emailForSignIn");
          // You can access the new user via result.user
          // Additional user info profile not available via:
          // result.additionalUserInfo.profile == null
          // You can check if the user is new or existing:
          // result.additionalUserInfo.isNewUser
        })
        .catch(function (error) {
          // Some error occurred, you can inspect the code: error.code
          // Common errors could be invalid email and invalid or expired OTPs.
        });
    }
  }

  public getUserData(): Promise<UserData | undefined> {
    return from(
      this.fs
        ?.doc(`user/${this.auth?.currentUser?.uid}`)
        .get()
        .then(
          (data: firebase.firestore.DocumentSnapshot): UserData => {
            const userData = data.data();
            return { id: this.auth?.currentUser?.uid, email: this.auth?.currentUser?.email, ...userData } as UserData;
          }
        ) || new Promise<undefined>((resolve) => resolve(undefined))
    ).toPromise();
  }

  public updateUserData(data: UserData): Promise<unknown> {
    return from(
      this.fs?.doc(`user/${this.auth?.currentUser?.uid}`).set(data) ||
      new Promise<void>((res) => {
        res();
      })
    )
      .pipe(
        tap(() => {
          from(this.getUserData()).subscribe((userData: UserData | undefined): void => this.onAuthStateChanged.next(userData));
        })
      )
      .toPromise();
  }

  public uploadFile(data: ArrayBuffer, fileName: string, metadata?: firebase.storage.UploadMetadata) {
    return this.storage
      ?.ref(`/files/${fileName}`)
      .put(data, metadata)
      .then((x) => `https://firebasestorage.googleapis.com/v0/b/${firebaseConfig.storageBucket}/o/${x.metadata.fullPath.replace("/", "%2F")}?alt=media`);
  }
}

export const firebaseService = new FirebaseService();

(window as any).f = firebaseService;
