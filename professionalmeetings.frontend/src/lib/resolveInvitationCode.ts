export function resolveInvitationCode() {
    return new URL(window.location.href).searchParams.get("invite");
}
