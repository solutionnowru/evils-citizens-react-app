import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
    typography: {
        h4: {
            color: "#515AA6",
        },
        h5: {
            color: "#0E133D",
        },
        subtitle1: {
            color: "#838383",
        },
    },
    palette: {
        primary: {
            main: "#FFB405",
            contrastText: "#FFFFFF",
        },
    },
});
