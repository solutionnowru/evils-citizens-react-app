import React, { useState, useEffect, useRef } from "react";
import { Typography, Button } from "@material-ui/core";
import { MyTextField } from "../components/@ui/MyTextField";
import { firebaseService } from "../lib/firebase/firebaseService";
import { UserData, userHasAllRequiredFields } from "../lib/UserData";
import { Redirect } from "react-router-dom";
import routes from "../routes";

const HomePage: React.FC = () => {
    const [user, setUser] = useState<UserData>();
    const linkREf = useRef<HTMLDivElement>(null);

    useEffect(() => {
        firebaseService.onAuthStateChanged.subscribe((user) => setUser(user));
    }, []);

    if (user && !userHasAllRequiredFields(user)) return <Redirect to={`${routes.introduction}?${new URL(window.location.search)}`} />;

    return (
        <div>
            <div style={{ display: "flex", alignItems: "center", flexDirection: "column", minHeight: "calc(100vh - 30px)", justifyContent: "center", padding: 15 }}>
                <img src="/img/logo.svg" alt="" />
                <br />
                <br />
                <Typography variant="h4" style={{ textAlign: "center" }}>
                    Поздравляем {user && `${user.personData.firstName} ${user.personData.lastName}`}!<br />
                    Вы стали одним из первых наших пользователей
                </Typography>
                <br />
                <br />
                <br />
                <img src="/img/2.svg" alt="" />
                <br />
                <br />
                <Typography variant="subtitle2" style={{ maxWidth: 590, textAlign: "center", color: "#838383" }}>
                    Спасибо за проявленный интерес!
                    <br />
                    Сейчас мы собираем базу первых пользователей для для того что бы ваши контакты были по настоящему полезными.
                    <br />
                    Мы про вас не забудем, следите за новостями, вам обеспечены бонусы!
                </Typography>
                <br />
                <br />
                <br />
                <Typography variant="h5">Пригласи друзей, чтобы получать контакты раньше!</Typography>
                <br />
                <br />
                <div style={{ display: "flex", alignItems: "center", flexDirection: "column", maxWidth: 400, width: "100%" }}>
                    <MyTextField value={`https://matchup-service.firebaseapp.com${user ? `?invite=${user?.id}` : ``}`} inputProps={{ ref: linkREf }} />
                    <br />
                    <Button
                        style={{ width: "100%" }}
                        variant="contained"
                        color="primary"
                        size="large"
                        onClick={() => {
                            (linkREf?.current as any).select();
                            document.execCommand("copy");
                        }}
                    >
                        Скопировать ссылку
                    </Button>
                    <br />
                    <Button
                        style={{ width: "100%", backgroundColor: "transparent" }}
                        variant="contained"
                        size="large"
                        onClick={() => window.open(`https://vk.com/share.php?url=https://matchup-service.firebaseapp.com${user ? `?invite=${user?.id}` : ``}`, "_blank")}
                    >
                        Поделиться ВКонтакте
                    </Button>
                    <br />
                    <Button
                        style={{ width: "100%", backgroundColor: "transparent" }}
                        variant="contained"
                        size="large"
                        onClick={() => window.open(`http://www.facebook.com/sharer.php?u=https://matchup-service.firebaseapp.com${user ? `?invite=${user?.id}` : ``}`, "_blank")}
                    >
                        Поделиться Facebook
                    </Button>
                    <br />
                    <Button
                        style={{ width: "100%", backgroundColor: "transparent" }}
                        variant="contained"
                        size="large"
                        onClick={() => window.open(`https://www.linkedin.com/shareArticle?mini=true&url=https://matchup-service.firebaseapp.com${user ? `?invite=${user?.id}` : ``}`, "_blank")}
                    >
                        Поделиться LinkedIn
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default HomePage;
