import React, { useState, ReactElement } from "react";
import { Container, Typography, Tabs, Tab, makeStyles } from "@material-ui/core";
import communications from "../../resourses/introduceYourselfImages/communications.svg";
import communications_inactive from "../../resourses/introduceYourselfImages/communications_inactive.svg";
import notebook from "../../resourses/introduceYourselfImages/notebook.svg";
import notebook_inactive from "../../resourses/introduceYourselfImages/notebook_inactive.svg";
import star from "../../resourses/introduceYourselfImages/star.svg";
import star_inactive from "../../resourses/introduceYourselfImages/star_inactive.svg";
import talk from "../../resourses/introduceYourselfImages/talk.svg";
import talk_inactive from "../../resourses/introduceYourselfImages/talk_inactive.svg";
import Interests from "./IntroduceYourselfStep/Interests";
import Contacts from "./IntroduceYourselfStep/Contacts";
import Status from "./IntroduceYourselfStep/Status";
import ConversationSubjects from "./IntroduceYourselfStep/ConversationSubjects";
import { PersonData } from "../../lib/UserData";

interface IntroduceYourselfStepProps {
    onDataUpdated: (data: PersonData) => void;
    data: PersonData;
}

enum IntroduceYourselfTabs {
    Interests = 0,
    Contacts = 1,
    Status = 2,
    Conversation = 3,
}

const useStyles = makeStyles({
    tabs: {
        marginBottom: "-1px",
        paddingLeft: "20px",
        paddingRight: "20px",
    },
    tabPanel: {
        border: "1px solid #DBDBDB",
        paddingTop: "45px",
        paddingBottom: "45px",
    },
});

const IntroduceYourselfStep: React.FC<IntroduceYourselfStepProps> = (props) => {
    const [personData, setPersonData] = useState<PersonData>(props.data);
    const [selectedTab, setSelectedTab] = useState<IntroduceYourselfTabs>(IntroduceYourselfTabs.Interests);
    const classes = useStyles();

    const updatePersonData = (newPersonData: PersonData) => {
        setPersonData(newPersonData);
        props.onDataUpdated(newPersonData);
    };

    const renderTab = (tab: IntroduceYourselfTabs, children: ReactElement) => (tab === selectedTab ? children : null);
    return (
        <>
            <Typography variant="h4">Расскажите немного о себе</Typography>
            <Container>
                <Tabs className={classes.tabs} value={selectedTab} onChange={(event, newTab) => setSelectedTab(newTab)}>
                    <Tab icon={selectedTab === IntroduceYourselfTabs.Interests ? <img src={star} alt="interests" /> : <img src={star_inactive} alt="interests" />} />
                    <Tab icon={selectedTab === IntroduceYourselfTabs.Contacts ? <img src={notebook} alt="contacts" /> : <img src={notebook_inactive} alt="contacts" />} />
                    <Tab icon={selectedTab === IntroduceYourselfTabs.Status ? <img src={communications} alt="status" /> : <img src={communications_inactive} alt="status" />} />
                    <Tab icon={selectedTab === IntroduceYourselfTabs.Conversation ? <img src={talk} alt="tips" /> : <img src={talk_inactive} alt="tips" />} />
                </Tabs>
                {renderTab(
                    IntroduceYourselfTabs.Interests,
                    <Container className={classes.tabPanel}>
                        <Typography variant="h5">Что вас интересует?</Typography>
                        <Typography variant="subtitle2">Добавьте или выберите из списка интересующие теги</Typography>
                        <Interests
                            selectedInterests={personData.interests}
                            onInterestsUpdated={(interests) => {
                                const newPersonData = { ...personData, interests: interests };
                                updatePersonData(newPersonData);
                            }}
                        />
                    </Container>
                )}
                {renderTab(
                    IntroduceYourselfTabs.Contacts,
                    <Container className={classes.tabPanel}>
                        <Typography variant="h5">О себе</Typography>
                        <Typography variant="subtitle2">Добавьте фотографию и ссылки на соц. сети</Typography>
                        <Contacts
                            data={props.data}
                            onDataUpdated={(contacts) => {
                                const newPersonData = { ...personData, ...contacts };
                                updatePersonData(newPersonData);
                            }}
                        />
                    </Container>
                )}
                {renderTab(
                    IntroduceYourselfTabs.Status,
                    <Container className={classes.tabPanel}>
                        <Typography variant="h5">Расскажите о себе</Typography>
                        <Typography variant="subtitle2">Чем бы Вы хотели поделиться со потенциальными контактами.</Typography>
                        <Status
                            status={props.data.status}
                            onStatusUpdated={(status) => {
                                const newPersonData = { ...personData, status: status };
                                updatePersonData(newPersonData);
                            }}
                        />
                    </Container>
                )}
                {renderTab(
                    IntroduceYourselfTabs.Conversation,
                    <Container className={classes.tabPanel}>
                        <Typography variant="h5">Давайте продолжим разговор!</Typography>
                        <Typography variant="subtitle2">При желании вы можете выбрать несколько источников разговора из списка ниже. Это поможет завязать разговор с вашим новым контактом.</Typography>
                        <ConversationSubjects
                            subjects={props.data.conversationSubjects || []}
                            onSubjectsUpdated={(subjects) => {
                                const newPersonData = { ...personData, conversationSubjects: subjects };
                                updatePersonData(newPersonData);
                            }}
                        />
                    </Container>
                )}
            </Container>
        </>
    );
};

export default IntroduceYourselfStep;
