import React, { useState } from "react";
import { Container, Button, TextField, makeStyles, CircularProgress } from "@material-ui/core";
import user from "../../../resourses/introduceYourselfImages/user.svg";
import InstagramIcon from "@material-ui/icons/Instagram";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import { firebaseService } from "../../../lib/firebase/firebaseService";
import { MyTextField } from "../../../components/@ui/MyTextField";

export interface ContactsData {
    twitter?: string;
    instagram?: string;
    facebook?: string;
    avatarUrl?: string
}

interface ContactsProps {
    data: ContactsData;
    onDataUpdated: (data: ContactsData) => void;
}

const useStyles = makeStyles({
    avatarContainer: { marginTop: "35px", position: 'relative' },
    avatarControlsContainer: {
        marginTop: "20px",
        display: "grid",
        gridTemplateColumns: "1fr 1fr",
        gridColumnGap: "14px",
        width: "min-content",
        "& > button": {
            width: "260px",
        },
    },
    textFieldsContainer: {
        marginTop: "48px",
        display: "grid",
        gridAutoRows: "min-content",
        gridTemplateColumns: "20px 70%",
        gridRowGap: "16px",
        gridColumnGap: "20px",
        justifyContent: "center",
        alignItems: "center",
    },
});

const Contacts: React.FC<ContactsProps> = (props) => {
    const [contacts, setContacts] = useState<ContactsData>(props.data);
    const [uploadingImage, setUploadingImage] = useState<boolean>();
    const classes = useStyles();

    // todo: formik, validation, avatar upload
    const updateContacts = (newContacts: ContactsData) => {
        setContacts(newContacts);
        props.onDataUpdated(newContacts);
    };
    const handleUploadButtonOnClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        event.preventDefault();
        event.stopPropagation();

        const input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('multiple', 'multiple');
        input.onchange = () => {
            const file = input.files && input.files[0];
            const reader = new FileReader();
            if (file) {
                reader.readAsArrayBuffer(file);
                reader.onload = () => {
                    if (reader.result instanceof ArrayBuffer) {
                        setUploadingImage(true);
                        firebaseService.uploadFile(
                            reader.result, file.name
                        )?.then(f => {
                            setUploadingImage(false);
                            updateContacts({ ...contacts, avatarUrl: f })
                        });
                    }
                };
            }
        };
        input.click();
    };

    return (
        <Container>
            <Container className={classes.avatarContainer}>
                {uploadingImage && <div
                    style={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        backgroundColor: '#f0f8ff73',
                        transform: 'translate(-50%, -50%)',
                        width: '110px',
                        height: '110px',
                    }}
                >
                    <CircularProgress />
                </div>
                }


                <img src={contacts.avatarUrl || user} alt="user" style={{ maxHeight: 110 }} />
            </Container>
            <Container className={classes.avatarControlsContainer}>
                {contacts.avatarUrl && <Button variant="contained" onClick={() => updateContacts({ ...contacts, avatarUrl: undefined })}>Удалить</Button>}
                <Button variant="contained" color="primary" onClick={handleUploadButtonOnClick}>
                    Загрузить
                </Button>
            </Container>
            <Container className={classes.textFieldsContainer}>
                <TwitterIcon /> <MyTextField value={contacts.twitter || ""} onChange={(e) => updateContacts({ ...contacts, twitter: e.target.value })} variant="outlined" />
                <InstagramIcon /> <MyTextField value={contacts.instagram || ""} onChange={(e) => updateContacts({ ...contacts, instagram: e.target.value })} variant="outlined" />
                <FacebookIcon /> <MyTextField value={contacts.facebook || ""} onChange={(e) => updateContacts({ ...contacts, facebook: e.target.value })} variant="outlined" />
            </Container>
        </Container>
    );
};

export default Contacts;
