import React, { useState, useEffect } from "react";
import padlock from "../../../resourses/conversationSubjectImages/padlock.svg";
import puzzle from "../../../resourses/conversationSubjectImages/puzzle.svg";
import stringInstrument from "../../../resourses/conversationSubjectImages/string-instrument.svg";
import technology from "../../../resourses/conversationSubjectImages/technology.svg";
import transport from "../../../resourses/conversationSubjectImages/transport.svg";
import { Container, makeStyles, Typography } from "@material-ui/core";
import { MyTextField } from "../../../components/@ui/MyTextField";
import _ from "lodash";
import { ConversationSubject } from "../../../lib/UserData";

interface ConversationSubjectViewData extends ConversationSubject {
    iconUrl: string;
    placeholder?: string;
}

const presetSubjects: ConversationSubjectViewData[] = [
    {
        title: "Я бы хотел узнать о...",
        iconUrl: puzzle,
        placeholder: "Изменении климата. Это реально беспокоит меня.",
    },
    {
        title: "Спроси меня о...",
        iconUrl: padlock,
        placeholder: "Спроси меня об инвестициях в криптовалюты в следующие несколько месяцев.",
    },
    {
        title: "На первом месте для меня...",
        iconUrl: technology,
        placeholder: "Увлекательные беседы с таинственными незнакомцами.",
    },
    {
        title: "Я недавно научился...",
        iconUrl: stringInstrument,
        placeholder: "Игре на виолончели. Очень хочу поделиться впечатлениями.",
    },
    {
        title: "Мои дополнительные интересы...",
        iconUrl: transport,
        placeholder: "Участие в онлайн-хакатонах.",
    },
];

const useStyles = makeStyles({
    subjectContainer: {
        cursor: "pointer",
        display: "grid",
        gridTemplateColumns: "68px auto",
        gridAutoRows: "minmax(65px,max-content)",
        alignItems: "center",
        justifyItems: "left",
        padding: "0",
        border: "1px solid #DBDBDB",
        marginTop: "12px",
        borderRadius: "4px",
        "& > img": {
            justifySelf: "center",
        },
    },
    subjectExpandableEditor: {
        gridColumnStart: "1",
        gridColumnEnd: "3",
    },
});

interface ConversationSubjectContainerProps {
    subject: ConversationSubjectViewData;
    onChange: (subject: ConversationSubjectViewData) => void;
    onClick: () => void;
    expanded: boolean;
}

const ConversationSubjectContainer: React.FC<ConversationSubjectContainerProps> = (props) => {
    const [subject, setSubject] = useState<string | undefined>(props.subject.subject);
    const classes = useStyles();

    useEffect(() => {
        if (props.subject.subject !== subject) props.onChange({ ...props.subject, subject: subject });
    }, [subject]);

    return (
        <Container className={classes.subjectContainer} onClick={props.onClick}>
            <img src={props.subject.iconUrl} alt={props.subject.title} />
            <Typography variant="h5">{props.subject.title}</Typography>
            {props.expanded && (
                <Container className={classes.subjectExpandableEditor}>
                    <MyTextField value={subject || ""} onChange={(e) => setSubject(e.target.value)} />
                </Container>
            )}
        </Container>
    );
};

interface ConversationSubjectsProps {
    subjects: ConversationSubject[];
    onSubjectsUpdated: (subjects: ConversationSubject[]) => void;
}

const ConversationSubjects: React.FC<ConversationSubjectsProps> = (props) => {
    const [subjects, setSubjects] = useState<ConversationSubject[]>(
        _(presetSubjects)
            .map((p) => {
                const existingSubject = props.subjects.find((s) => s.title === p.title);
                if (existingSubject !== undefined) {
                    p.subject = existingSubject.subject;
                }

                return p;
            })
            .orderBy((p) => p.title)
            .value()
    );
    const [expandedSubject, setExpandedSubject] = useState<string>();
    const classes = useStyles();

    return (
        <>
            {presetSubjects.map((s, i) => (
                <ConversationSubjectContainer
                    key={i}
                    expanded={s.title === expandedSubject}
                    onClick={() => setExpandedSubject(s.title)}
                    subject={s}
                    onChange={(subject) => {
                        const newSubjects = _([...subjects.filter((s) => s.title !== subject.title), subject])
                            .orderBy((s) => s.title)
                            .value();

                        setSubjects(newSubjects);
                        props.onSubjectsUpdated(
                            _(newSubjects)
                                .filter((s) => s.subject !== undefined)
                                .map((s) => ({
                                    title: s.title,
                                    subject: s.subject,
                                }))
                                .value()
                        );
                    }}
                />
            ))}
        </>
    );
};

export default ConversationSubjects;
