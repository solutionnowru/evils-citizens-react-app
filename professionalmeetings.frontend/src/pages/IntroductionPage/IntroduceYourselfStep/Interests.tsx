import React, { useState } from "react";
import TagSelector, { Tag } from "../../../components/TagSelector/TagSelector";
import _ from "lodash";

const presetTags: Tag[] = [
    { group: "Бизнес", tag: "инвестирование" },
    { group: "Бизнес", tag: "бизнес" },
    { group: "Бизнес", tag: "консультирование" },
    { group: "Бизнес", tag: "здравоохранение" },
    { group: "Бизнес", tag: "продажи" },
    { group: "Бизнес", tag: "предпринимательство" },
    { group: "Бизнес", tag: "образование" },
    { group: "Бизнес", tag: "маркетинг" },
    { group: "Бизнес", tag: "СМИ" },
    { group: "Бизнес", tag: "недвижимость" },
    { group: "Науки и технологии", tag: "дизайн продукта" },
    { group: "Науки и технологии", tag: "биохакинг" },
    { group: "Науки и технологии", tag: "машинное обучение" },
    { group: "Науки и технологии", tag: "языки программирования" },
    { group: "Науки и технологии", tag: "крипто" },
    { group: "Науки и технологии", tag: "наука о данных" },
    { group: "Науки и технологии", tag: "продуктовый менеджер" },
    { group: "Науки и технологии", tag: "quant finance" },
    { group: "Науки и технологии", tag: "vr / ar" },
    { group: "Науки и технологии", tag: "робототехника" },
    { group: "Науки и технологии", tag: "visual design" },
    { group: "Науки и технологии", tag: "науки о жизни" },
    { group: "Общество", tag: "фотография" },
    { group: "Общество", tag: "путешествия" },
    { group: "Общество", tag: "фитнес" },
    { group: "Общество", tag: "питание" },
    { group: "Общество", tag: "музыка" },
    { group: "Общество", tag: "кино" },
    { group: "Общество", tag: "чтение" },
    { group: "Общество", tag: "письмо" },
    { group: "Общество", tag: "спорт" },
    { group: "Общество", tag: "прогулки" },
    { group: "Общество", tag: "концерты" },
    { group: "Общество", tag: "видеоигры" },
    { group: "Общество", tag: "лыжи" },
    { group: "Общество", tag: "кинематограф" },
    { group: "Общество", tag: "сноуборд" },
    { group: "Общество", tag: "покер" },
    { group: "Общество", tag: "рыбалка" },
    { group: "Общество", tag: "воспитание детей" },
    { group: "Общество", tag: "мода" },
    { group: "Общество", tag: "обеды" },
    { group: "Общество", tag: "готовка" },
    { group: "Общество", tag: "оздоровление" },
    { group: "Общество", tag: "устойчивое развитие" },
    { group: "Общество", tag: "бег" },
];

interface IntererstsProps {
    onInterestsUpdated: (interests: string[]) => void;
    selectedInterests?: string[];
}

const Interests: React.FC<IntererstsProps> = (props) => {
    const [tags, setTags] = useState<Tag[]>([
        ...presetTags,
        ...(props.selectedInterests
            ? _(props.selectedInterests)
                  .filter((i) => presetTags.find((t) => t.tag === i) === undefined)
                  .map((i) => ({ tag: i }))
                  .value()
            : []),
    ]);

    return (
        <>
            <TagSelector
                onTagAdded={(tag) => {
                    props.onInterestsUpdated([...(props.selectedInterests || []), tag]);
                    setTags([...tags, { tag: tag }]);
                }}
                tags={tags}
                isTagSelected={(tag) => props.selectedInterests !== undefined && props.selectedInterests.indexOf(tag.tag) >= 0}
                onTagsSelected={(tags) => {
                    props.onInterestsUpdated(tags.map((t) => t.tag));
                }}
            />
        </>
    );
};

export default Interests;
