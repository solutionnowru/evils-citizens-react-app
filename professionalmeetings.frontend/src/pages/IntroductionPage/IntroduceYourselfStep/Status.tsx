import React from "react";
import { TextField, Container, Typography, Paper, makeStyles, Grid } from "@material-ui/core";
import user from "../../../resourses/introduceYourselfImages/user.svg";

interface StatusProps {
    status?: string;
    onStatusUpdated: (status: string) => void;
}

const useStyles = makeStyles((theme) => ({
    exampleAvatar: {
        height: "32px",
    },
    examplesContainer: {
        display: "grid",
        gridTemplateColumns: "32px auto",
        gridAutoRows: "max-content",
        gridRowGap: "10px",
        gridColumnGap: "24px",
        alignItems: "center",
    },
    exampleQuote: {
        backgroundColor: theme.palette.grey[100],
        textAlign: "left",
        padding: "16px 10px 10px 16px",
        color: theme.palette.grey[700],
        boxShadow: "none",
    },
    exampleTitle: {
        textAlign: "left",
        color: theme.palette.grey[700],
        marginBottom: "20px",
    },
    statusInput: {
        width: "100%",
    },
    statusInputContainer: {
        marginTop: "20px",
        display: "grid",
        gridAutoRows: "auto",
        justifyItems: "right",
    },
}));

const Status: React.FC<StatusProps> = (props) => {
    const classes = useStyles();

    // todo: validation

    return (
        <>
            <Container className={classes.statusInputContainer}>
                <TextField multiline variant="outlined" value={props.status || ""} onChange={(e) => props.onStatusUpdated(e.target.value)} className={classes.statusInput} />
                <Typography variant="caption">0 (Минимум 15 символов)</Typography>
            </Container>

            <Container>
                <Typography className={classes.exampleTitle} variant="h5">
                    Примеры
                </Typography>
                <Container className={classes.examplesContainer}>
                    <img className={classes.exampleAvatar} src={user} alt="avatar" />
                    <Paper className={classes.exampleQuote}>Ким дизайнер в Facebook, который пишет о дизайне в повседневной жизни. Она ищет людей, которые любят работать над впечатляющими продуктами.</Paper>
                    <img className={classes.exampleAvatar} src={user} alt="avatar" />
                    <Paper className={classes.exampleQuote}>Сами - инженер-механик, который ищет людей, чтобы поговорить об его новой идее стартапа.</Paper>
                    <img className={classes.exampleAvatar} src={user} alt="avatar" />
                    <Paper className={classes.exampleQuote}>Омар-оперный певец, который пишет свой собственный блог.</Paper>
                </Container>
            </Container>
        </>
    );
};

export default Status;
