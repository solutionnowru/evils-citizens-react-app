import React, { useState, ReactElement, useEffect } from "react";
import SelectCityStep from "./SelectCityStep";
import { Container, makeStyles, Stepper, StepLabel, Step, StepIconProps } from "@material-ui/core";
import SelectObjectivesStep from "./SelectObjectivesStep";
import { Button } from "../../components/Button";
import location from "../../resourses/introductionSteps/location.svg";
import heart from "../../resourses/introductionSteps/heart.svg";
import user from "../../resourses/introductionSteps/user.svg";
import user_inactive from "../../resourses/introductionSteps/user_inactive.svg";
import flag_inactive from "../../resourses/introductionSteps/flag_inactive.svg";
import IntroduceYourselfStep from "./IntroduceYourselfStep";
import LoginDataStep from "./LoginDataStep";
import { userHasAllRequiredFields, resolveValidationMessage, UserData } from "../../lib/UserData";
import routes from "../../routes";
import { Redirect, useHistory } from "react-router-dom";
import { firebaseService } from "../../lib/firebase/firebaseService";

enum IntroductionStep {
    LoginData = 0,
    City = 1,
    Objectives = 2,
    SelfIntroduction = 3,
    Finish = 4,
}

const useStyles = makeStyles({
    outerContainer: {
        display: "grid",
        justifyContent: "center",
        alignContent: "center",
        margin: "auto",
        height: "100%",
    },
    innerContainer: {
        display: "grid",
        gridTemplateColumns: "auto",
        justifyItems: "center",
        gridTemplateRows: "minmax(70px, max-content) min-content",
        gridAutoRows: "70px",
        textAlign: "center",
        "& > div:first-of-type": {
            marginBottom: "20px",
        },
    },
    navigationContainer: {
        marginTop: "20px",
        display: "grid",
        gridTemplateColumns: "1fr 1fr",
        gridColumnGap: "14px",
        width: "auto",
        "& > button": {
            width: "260px",
        },
    },
});

interface IntroductionPageInnerProps {
    data: UserData;
}

const IntroductionPageInner: React.FC<IntroductionPageInnerProps> = (props) => {
    const prepareUserData = (data: UserData) => {
        if (!data.personData)
            data.personData = {
                conversationSubjects: [],
                facebook: "",
                firstName: "",
                instagram: "",
                interests: [],
                invitationCode: "",
                lastName: "",
                profileImageUrl: "",
                status: "",
                twitter: "",
            };

        return data;
    };

    const classes = useStyles();
    const [userData, setUserData] = useState<UserData>(prepareUserData(props.data));
    const [step, setStep] = useState<IntroductionStep>(IntroductionStep.LoginData);

    const resolveStepIcon = (props: StepIconProps) => {
        const isActive = props.icon === step;
        switch (props.icon) {
            case IntroductionStep.City:
                return <img src={location} alt="location" />;
            case IntroductionStep.Objectives:
                return <img src={heart} alt="interests" />;
            case IntroductionStep.SelfIntroduction:
                return isActive ? <img src={user} alt="self" /> : <img src={user_inactive} alt="self" />;
            case IntroductionStep.Finish:
                return <img src={flag_inactive} alt="finish" />;
            default:
                return null;
        }
    };

    const renderStepWithNavigation = (component: ReactElement) => (
        <>
            <Container>
                <Stepper activeStep={step}>
                    <Step key={IntroductionStep.City}>
                        <StepLabel StepIconComponent={resolveStepIcon}></StepLabel>
                    </Step>
                    <Step key={IntroductionStep.Objectives}>
                        <StepLabel StepIconComponent={resolveStepIcon}></StepLabel>
                    </Step>
                    <Step key={IntroductionStep.SelfIntroduction}>
                        <StepLabel StepIconComponent={resolveStepIcon}></StepLabel>
                    </Step>
                    <Step key={IntroductionStep.Finish}>
                        <StepLabel StepIconComponent={resolveStepIcon}></StepLabel>
                    </Step>
                </Stepper>
            </Container>
            <Container className={classes.innerContainer}>{component}</Container>
            <Container className={classes.navigationContainer}>
                <Button variant="outlined" onClick={() => setStep(step - 1)}>
                    Назад
                </Button>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                        firebaseService.updateUserData(userData);
                        setStep(step + 1);
                    }}
                >
                    Далее
                </Button>
            </Container>
        </>
    );

    const resolveStep = () => {
        switch (step) {
            case IntroductionStep.LoginData:
                return (
                    <Container className={classes.innerContainer}>
                        <LoginDataStep
                            data={userData.personData}
                            onLoginDataUpdated={(data) => {
                                setUserData({
                                    ...userData,
                                    personData: {
                                        ...userData.personData,
                                        firstName: data.firstName,
                                        lastName: data.lastName,
                                        invitationCode: data.invitationCode,
                                    },
                                });
                                setStep(IntroductionStep.City);
                            }}
                        />
                    </Container>
                );

            case IntroductionStep.City:
                return (
                    <Container className={classes.innerContainer}>
                        <SelectCityStep
                            onStepComplete={(cityName) => {
                                setUserData({ ...userData, cityName: cityName });
                                setStep(IntroductionStep.Objectives);
                            }}
                        />
                    </Container>
                );

            case IntroductionStep.Objectives:
                return renderStepWithNavigation(
                    <SelectObjectivesStep
                        selectedObjectives={userData.objectives}
                        onObjectivesSelected={(objectives) => {
                            setUserData({
                                ...userData,
                                objectives: objectives,
                            });
                        }}
                    />
                );

            case IntroductionStep.SelfIntroduction:
                return renderStepWithNavigation(<IntroduceYourselfStep onDataUpdated={(d) => setUserData({ ...userData, personData: d })} data={userData.personData} />);

            case IntroductionStep.Finish:
                if (userHasAllRequiredFields(userData)) return <Redirect to={routes.home} />;
                else {
                    const validationMessage = resolveValidationMessage(userData);
                    alert(validationMessage);
                    setStep(IntroductionStep.LoginData);
                    break;
                }

            default:
                return null;
        }
    };

    return <Container className={classes.outerContainer}>{resolveStep()}</Container>;
};

const IntroductionPage: React.FC = () => {
    const [userData, setUserData] = useState<UserData>();
    const history = useHistory();

    useEffect(() => {
        firebaseService.onAuthStateChanged.subscribe(user => {
            setUserData(user);
        })
    }, []);
    if (userData) return <IntroductionPageInner data={userData} />;
    else return <>Wait...</>;
};

export default IntroductionPage;
