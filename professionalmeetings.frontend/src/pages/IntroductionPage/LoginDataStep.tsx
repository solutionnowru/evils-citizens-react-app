import React, { useState } from "react";
import { MyTextField } from "./../../components/@ui/MyTextField";
import { Button } from "@material-ui/core";
import { resolveInvitationCode } from "../../lib/resolveInvitationCode";
import loginBanner from "../../resourses/loginBanner.svg";

interface LoginData {
    firstName?: string;
    lastName?: string;
    invitationCode?: string;
    readyPay?: boolean
}

interface LoginDataStepProps {
    data: LoginData;
    onLoginDataUpdated: (data: LoginData) => void;
}

const LoginDataStep: React.FC<LoginDataStepProps> = (props) => {
    const addInvitationCode = (data: LoginData) => {
        const invitationCode = resolveInvitationCode();
        if (invitationCode) data.invitationCode = invitationCode;
        const readyPay = new URL(window.location.href).searchParams.get("readypay");
        if (readyPay) data.readyPay = !!readyPay;

        return data;
    };

    const [loginData, setLoginData] = useState<LoginData>(addInvitationCode(props.data));

    return (
        <>
            <div style={{ width: 580, display: "flex", flexDirection: "column" }}>
                <img src={loginBanner} />
            </div>
            <div style={{ width: 380, display: "flex", flexDirection: "column" }}>
                <MyTextField value={loginData.firstName || ""} onChange={(e) => setLoginData({ firstName: e.target.value, lastName: loginData.lastName, invitationCode: loginData.invitationCode })} placeholder="Имя" />
                <br />
                <MyTextField value={loginData.lastName || ""} onChange={(e) => setLoginData({ firstName: loginData.firstName, lastName: e.target.value, invitationCode: loginData.invitationCode })} placeholder="Фамилия" />
                <br />
                <MyTextField
                    value={loginData.invitationCode || ""}
                    onChange={(e) => setLoginData({ firstName: loginData.firstName, lastName: loginData.lastName, invitationCode: e.target.value })}
                    placeholder="Пригласительный код (если есть)"
                />
                <br />
                <Button style={{ width: "100%" }} variant="contained" color="primary" onClick={() => props.onLoginDataUpdated(loginData)}>
                    Начать
                </Button>
            </div>
        </>
    );
};

export default LoginDataStep;
