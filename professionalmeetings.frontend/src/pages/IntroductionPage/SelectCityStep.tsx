import React from "react";
import { Typography, Container, Link, Grid, createStyles, makeStyles } from "@material-ui/core";
import TileSelector from "../../components/TileSelector/TileSelector";
import { TileData } from "../../components/TileSelector/Tile";
import moskowIcon from "../../resourses/cityImages/moskow.png";
import piterIcon from "../../resourses/cityImages/piter.png";

const cities: TileData[] = [
    {
        label: "Москва",
        iconUrl: moskowIcon,
    },
    {
        label: "Санкт-Петербург",
        iconUrl: piterIcon,
    },
];

interface SelectCityStepProps {
    onStepComplete: (cityName: string) => void;
}

const useStyles = makeStyles({
    otherCityLink: {
        alignSelf: "end",
    },
});

const SelectCityStep: React.FC<SelectCityStepProps> = (props) => {
    const classes = useStyles();

    return (
        <>
            <Container>
                <Typography variant="h4">В каком вы городе?</Typography>
                <Typography variant="subtitle1">Дайте нам знать, где ваша база</Typography>
            </Container>
            <TileSelector
                styles={{
                    gridColumnGap: "50px",
                }}
                tileStyles={{
                    padding: "24 36 24 24",
                    boxShadow: "0 0 50px 0 rgba(0,0,0,0.1)",
                    width: 275,
                    height: 367,
                    "&:hover": { boxShadow: "0 0 25px 0 rgba(0,0,0,0.2)" },
                }}
                tiles={cities}
                onTileSelected={(tiles) => props.onStepComplete(tiles[0].label)}
                columnCount={2}
            />
            <Link className={classes.otherCityLink} href="javascript:void(0)" onClick={() => props.onStepComplete("Другой")}>
                Другой
            </Link>
        </>
    );
};

export default SelectCityStep;
