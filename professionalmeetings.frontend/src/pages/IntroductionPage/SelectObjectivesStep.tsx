import React from "react";
import { TileData } from "../../components/TileSelector/Tile";
import { Container, Typography } from "@material-ui/core";
import TileSelector from "../../components/TileSelector/TileSelector";
import think from "../../resourses/objectiveImages/think.svg";
import teamwork from "../../resourses/objectiveImages/teamwork.svg";
import team from "../../resourses/objectiveImages/team.svg";
import curriculum from "../../resourses/objectiveImages/curriculum.svg";
import bag from "../../resourses/objectiveImages/bag.svg";
import dollar from "../../resourses/objectiveImages/dollar.svg";
import space from "../../resourses/objectiveImages/space.svg";
import presentation from "../../resourses/objectiveImages/presentation.svg";
import calendar from "../../resourses/objectiveImages/calendar.svg";
import business from "../../resourses/objectiveImages/business.svg";
import laptop from "../../resourses/objectiveImages/laptop.svg";
import puzzle from "../../resourses/objectiveImages/puzzle.svg";

const objectives: TileData[] = [
    {
        label: "Мозговой штурм",
        iconUrl: think,
    },
    {
        label: "Развивать свою команду",
        iconUrl: teamwork,
    },
    {
        label: "Основать компанию",
        iconUrl: team,
    },
    {
        label: "Исследовать других",
        iconUrl: curriculum,
    },
    {
        label: "Сделать бизнес",
        iconUrl: bag,
    },
    {
        label: "Инвестировать",
        iconUrl: dollar,
    },
    {
        label: "Исследовать новое",
        iconUrl: space,
    },
    {
        label: "Быть наставником",
        iconUrl: presentation,
    },
    {
        label: "Организовать мероприятие",
        iconUrl: calendar,
    },
    {
        label: "Привлечь финансирование",
        iconUrl: business,
    },
    {
        label: "Поиск соучредителя",
        iconUrl: laptop,
    },
    {
        label: "Знакомства",
        iconUrl: puzzle,
    },
];

interface SelectObjectivesStepProps {
    selectedObjectives?: string[];
    onObjectivesSelected: (objectives: string[]) => void;
}

const SelectObjectivesStep: React.FC<SelectObjectivesStepProps> = (props) => {
    return (
        <>
            <Container>
                <Typography variant="h4">Ваши цели?</Typography>
                <Typography variant="subtitle1">Выберите до 3-х целей. Они будут храниться в тайне от других пользователей, но помогут нам найти соответствующие матчи.</Typography>
            </Container>
            <TileSelector
                styles={{
                    gridColumnGap: "8px",
                    gridRowGap: "8px",
                }}
                tileStyles={{
                    width: 140,
                    height: 140,
                    border: "1px solid #DBDBDB",
                    gridTemplateRows: "auto 40px",
                    boxShadow: "none",
                    "&:hover": { boxShadow: "0 0 5px 0 rgba(0,0,0,0.2)" },
                    "& > img": { height: "60px", alignSelf: "center" },
                }}
                tiles={objectives}
                onTileSelected={(tiles) => props.onObjectivesSelected(tiles.map((t) => t.label))}
                columnCount={4}
                maximumSelectedTileCount={3}
                isTileSelected={(t) => props.selectedObjectives !== undefined && props.selectedObjectives.find((o) => o === t.label) !== undefined}
            />
        </>
    );
};

export default SelectObjectivesStep;
