import React, { useEffect } from "react";
import { firebaseService } from "../lib/firebase/firebaseService";
import { useHistory } from "react-router-dom";
import { resolveInvitationCode } from "../lib/resolveInvitationCode";

const SignInEmailFinishPage: React.FC = () => {
    const history = useHistory();
    useEffect(() => {
        (async () => {
            await firebaseService.finishSugnInEmalLink();
            const invitationCode = resolveInvitationCode();
            const readyPay = new URL(window.location.href).searchParams.get("readypay");

            if (invitationCode) history.push("/introduction?invite=" + invitationCode + `${readyPay ? `&readypay=${readyPay}` : ''}`);
            else history.push("/introduction" + `${readyPay ? `?readypay=${readyPay}` : ''}`);
        })();
    }, []);
    return <div />;
};

export default SignInEmailFinishPage;
