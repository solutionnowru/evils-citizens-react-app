import React, { useState, useEffect } from "react";
import { firebaseService } from "../lib/firebase/firebaseService";
import { CircularProgress, Typography, Container } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { resolveInvitationCode } from "../lib/resolveInvitationCode";

const SignInEmailStartPage: React.FC = () => {
    const [isSendEmail, setIsSendEmail] = useState<boolean>();
    const history = useHistory();
    const send = async () => {
        let email = new URL(window.location.href).searchParams.get("email");
        const invite = new URL(window.location.href).searchParams.get("invite");
        const readyPay = new URL(window.location.href).searchParams.get("readypay");
        if (!email) {
            email = window.prompt("Что то пошло не так и ваш email где то потерялся((( Введите пожалйста еще раз.");
        }

        if (!email) {
            alert("Ошибка ((");
            return;
        }
        try {
            await firebaseService.sendSignInLinkToEmail(email, invite || undefined, readyPay || undefined);
        } catch (err) {
            console.log(err);
            alert("Ошибка отправки email ((");
        }
        setIsSendEmail(true);
    };

    useEffect(() => {
        send();
    }, []);

    return (
        <>
            {!isSendEmail && <CircularProgress />}
            {isSendEmail && (
                <>
                    <Container
                        style={{
                            justifyItems: "center",
                            display: "grid",
                            height: "100%",
                            alignContent: "center",
                            gridAutoRows: "min-content",
                        }}
                    >
                        <Typography variant="h4">Спасибо за регистрацию!</Typography>
                        <Typography variant="h5">Вам на почту придет ссылка для продолжения регистрации</Typography>
                    </Container>
                </>
            )}
        </>
    );
};

export default SignInEmailStartPage;
