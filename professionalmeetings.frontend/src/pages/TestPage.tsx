import React, { useState, useEffect } from "react";
import { firebaseService } from "../lib/firebase/firebaseService";

const TestPage: React.FC = () => {
  const [email, setEmail] = useState<string>('');
  
  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    (async () => {
      if (email) {
        try {
          await firebaseService.sendSignInLinkToEmail(email);
          alert('done')
        } catch (err) {
          alert('failed')
        }
      }
    })()
  }

  useEffect(() => {
    (async () => {
      await firebaseService.finishSugnInEmalLink()
      alert('done');
    })();
    
  },[])

  return <div>
    <form onSubmit={onSubmit}>
      <input type="email" onChange={(event) => setEmail(event.target.value)} value={email} name="email"/>
      <button type="submit">submit</button>
    </form>
  </div>
}

export default TestPage;

