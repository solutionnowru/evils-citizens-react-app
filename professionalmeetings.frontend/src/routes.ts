const routes = {
    home: "/",
    signInEmailStart: "/sign-in-email-link-start",
    signInEmailFinish: "/sign-in-email-link-finish",
    introduction: "/introduction",

};

export default routes;
